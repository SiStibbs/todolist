package net.stibbards.todolist.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import net.stibbards.todolist.App;
import net.stibbards.todolist.R;
import net.stibbards.todolist.database.ToDoItem;
import net.stibbards.todolist.databinding.FragmentMainBinding;
import net.stibbards.todolist.utils.SwipeToDeleteCallback;
import net.stibbards.todolist.utils.ToDoItemAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainFragment extends Fragment {

    private FragmentMainBinding binding;
    private RecyclerView listView;
    private ToDoItemAdapter adapter;

    private String currentItemId;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    )

    {

        binding = FragmentMainBinding.inflate(inflater, container, false);
        listView = binding.list;

        binding.overlay.setOnClickListener(view -> binding.overlay.setVisibility(View.GONE));

        binding.fab.setOnClickListener(view -> {
            App.db.itemDao().insertAll(new ToDoItem());
            adapter.notifyChange();
        });

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new ToDoItemAdapter(getContext());
        App.db.itemDao().getAll().observe(getViewLifecycleOwner(), itemList -> adapter.setItems(itemList));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(listView);
        listView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ToDoItemAdapter.OnItemClickListener() {
            @Override
            public void onImageButtonClick(String id) {
                showImageDialog(id);
            }

            @Override
            public void onImageClick(String id) {
                showImagePreview(id);
            }
        });
    }

    private void showImagePreview(String id) {
        ToDoItem item = App.db.itemDao().findById(id);
        if(item.itemImage != null) {
            View overlay = binding.overlay;
            overlay.setVisibility(View.VISIBLE);

            ImageView imageFull = binding.imageFull;
            Picasso.get()
                    .load(new File(item.itemImage))
                    .resize(Resources.getSystem().getDisplayMetrics().widthPixels, 0)
                    .into(imageFull);
        }
    }

    private void showImageDialog(final String id) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }
        else {
            currentItemId = id;
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.add_image)
                    .setMessage(R.string.camera_gallery)
                    .setCancelable(true)

                    .setPositiveButton(R.string.camera, (dialog, which) -> {
                        getCameraImage();
                    })

                    .setNegativeButton(R.string.gallery, (dialog, which) -> {
                        getGalleryImage();
                    })

                    .setNeutralButton(R.string.delete, (dialog, which) -> {
                        ToDoItem item = App.db.itemDao().findById(id);
                        item.itemImage = null;
                        item.updated = System.currentTimeMillis();
                        App.db.itemDao().insertAll(item);
                        adapter.notifyChange();
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    private void getCameraImage() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);
    }

    private void getGalleryImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if(imageReturnedIntent != null) {

            Bitmap bitmap;
            String imgPath;
            File destination;

            switch (requestCode) {
                case 0:
                    if (resultCode == Activity.RESULT_OK) {
                        bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                        File path = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name));
                        path.mkdirs();
                        destination = new File(path, currentItemId + ".jpg");
                        FileOutputStream fo;
                        try {
                            if(!destination.exists()) {
                                destination.createNewFile();
                            }
                            fo = new FileOutputStream(destination);
                            fo.write(bytes.toByteArray());
                            fo.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        imgPath = destination.getAbsolutePath();
                        ToDoItem item = App.db.itemDao().findById(currentItemId);
                        item.itemImage = imgPath;
                        Picasso.get().invalidate(destination);
                        App.db.itemDao().insertAll(item);
                        adapter.notifyChange();
                    }

                    break;
                case 1:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri selectedImage = imageReturnedIntent.getData();
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImage);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                            imgPath = getRealPathFromURI(selectedImage);
                            ToDoItem item = App.db.itemDao().findById(currentItemId);
                            item.itemImage = imgPath;
                            item.updated = System.currentTimeMillis();
                            Picasso.get().invalidate(selectedImage);
                            App.db.itemDao().insertAll(item);
                            adapter.notifyChange();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) throws Exception {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}