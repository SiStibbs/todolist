package net.stibbards.todolist;

import android.app.Application;

import androidx.room.Room;

import net.stibbards.todolist.database.AppDatabase;

public class App extends Application {

    public static AppDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();

        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database").allowMainThreadQueries().build();
        //db.itemDao().deleteAll();//insertAll(new ToDoItem(false, "Do something"), new ToDoItem(true, "Already done this"));
    }
}
