package net.stibbards.todolist.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ToDoItemDao {

    @Query("SELECT * FROM todoitem ORDER BY isChecked, created")
    LiveData<List<ToDoItem>> getAll();

    @Query("SELECT * FROM todoitem")
    List<ToDoItem> getItems();

    @Query("SELECT * FROM todoitem WHERE id IN (:userIds)")
    List<ToDoItem> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM todoitem WHERE id = :id LIMIT 1")
    ToDoItem findById(String id);

    @Query("SELECT * FROM todoitem WHERE itemName LIKE :name LIMIT 1")
    ToDoItem findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ToDoItem... items);

    @Delete
    void delete(ToDoItem item);

    @Query("DELETE FROM todoitem")
    void deleteAll();
}
