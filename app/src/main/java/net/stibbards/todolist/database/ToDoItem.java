package net.stibbards.todolist.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity
public class ToDoItem {
    @NonNull
    @PrimaryKey
    public String id;

    @ColumnInfo(name = "isChecked")
    public boolean isChecked;

    @ColumnInfo(name = "itemName")
    public String itemName;

    @ColumnInfo(name = "created")
    public long created;

    @ColumnInfo(name = "updated")
    public long updated;

    @ColumnInfo(name = "itemImage")
    public String itemImage;

    public ToDoItem() {
        id = UUID.randomUUID().toString();
        created = System.currentTimeMillis();
        updated = System.currentTimeMillis();
    }
}
