package net.stibbards.todolist.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import net.stibbards.todolist.App;
import net.stibbards.todolist.R;
import net.stibbards.todolist.database.ToDoItem;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ToDoItemAdapter extends RecyclerView.Adapter<ToDoItemAdapter.ViewHolder> {

    private List<ToDoItem> itemList;
    private final SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM, ''yy hh:mm aaa", Locale.getDefault());
    private final Context context;
    private OnItemClickListener onItemClickListener;

    public ToDoItemAdapter(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setItems(List<ToDoItem> items) {
        this.itemList = items;
        notifyDataSetChanged();
    }

    public void notifyChange() {
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        if(position < itemList.size()) {
            ToDoItem item = itemList.remove(position);
            App.db.itemDao().delete(item);
            notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onImageButtonClick(String id);
        void onImageClick(String id);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.todo_item_layout, viewGroup, false);

        return new ViewHolder(view, new CheckChangeListener(), new FocusListener());
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View root;
        public final EditText itemName;
        public final CheckBox itemCheck;
        public final TextView itemUpdated;
        public final CheckChangeListener checkListener;
        public final FocusListener focusListener;
        public final ImageView itemImage;
        public final ImageButton itemImageButton;

        public ViewHolder(View view, CheckChangeListener checkListener, FocusListener focusListener) {
            super(view);
            // Define click listener for the ViewHolder's View
            root = view;
            itemName = view.findViewById(R.id.itemName);
            itemCheck = view.findViewById(R.id.itemCheck);
            itemUpdated = view.findViewById(R.id.itemUpdated);
            this.checkListener = checkListener;
            itemCheck.setOnCheckedChangeListener(checkListener);
            this.focusListener = focusListener;
            itemName.setOnFocusChangeListener(focusListener);

            itemImage = view.findViewById(R.id.itemImage);
            itemImageButton = view.findViewById(R.id.itemImageButton);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        ToDoItem item = itemList.get(position);
        viewHolder.checkListener.setRedrawing(true);
        viewHolder.itemName.clearFocus();
        viewHolder.itemName.setText(item.itemName);
        viewHolder.itemCheck.setChecked(item.isChecked);
        viewHolder.itemUpdated.setText(sdf.format(new Date(item.updated)));
        viewHolder.checkListener.updatePosition(position);
        viewHolder.focusListener.updatePosition(position);

        viewHolder.itemImageButton.setOnClickListener(view -> onItemClickListener.onImageButtonClick(item.id));
        viewHolder.itemImage.setOnClickListener(view -> onItemClickListener.onImageClick(item.id));
        viewHolder.checkListener.setRedrawing(false);

        viewHolder.itemName.setTextColor(item.isChecked ? Color.LTGRAY : Color.BLACK);
        viewHolder.root.setBackgroundColor(item.isChecked ? ContextCompat.getColor(context, R.color.light_grey) : Color.WHITE);

        viewHolder.itemImage.setVisibility(item.itemImage == null ? View.GONE : View.VISIBLE);
        if(item.itemImage != null) {
            Picasso.get()
                    .load(new File(item.itemImage))
                    .resize(80, 80)
                    .centerCrop()
                    .into(viewHolder.itemImage);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    private class FocusListener implements View.OnFocusChangeListener {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void onFocusChange(View v, boolean isFocused) {
            if(!isFocused) {
                ToDoItem item = itemList.get(position);
                item.itemName = ((TextView) v).getText().toString();
                item.updated = System.currentTimeMillis();
                App.db.itemDao().insertAll(item);
            }
        }
    }

    private class CheckChangeListener implements CompoundButton.OnCheckedChangeListener {
        private int position;
        private boolean isRedrawing;

        public void updatePosition(int position) {
            this.position = position;
        }

        public void setRedrawing(boolean redrawing) {
            this.isRedrawing = redrawing;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if(!isRedrawing) {
                ToDoItem item = itemList.get(position);

                item.isChecked = isChecked;
                item.updated = System.currentTimeMillis();
                App.db.itemDao().insertAll(item);
                notifyItemChanged(position);
            }
        }
    }
}
